import React, {Fragment} from 'react';
import {Text, View} from 'react-native';
import {Header} from 'react-native-elements';
export default class WelcomeScreen extends React.Component {
  //Profile Screen to show from Open profile button
  render() {
    return (
      <Fragment>
        <Header
          statusBarProps={{barStyle: 'light-content'}}
          leftComponent={{icon: 'play-circle-outline', color: '#fff'}}
          centerComponent={{
            text: 'STREAMING BMN',
            style: {
              color: '#fff',
              fontSize: 20,
              fontWeight: 'bold',
            },
          }}
          containerStyle={{
            backgroundColor: '#3D6DCC',
            justifyContent: 'space-around',
          }}
        />
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <Text>Welcome!</Text>
        </View>
      </Fragment>
    );
  }
}
