import React, {Fragment} from 'react';
import {Button, Icon, Image, Text, Header} from 'react-native-elements';
import {Linking} from 'react-native';
import {
  StyleSheet,
  ScrollView,
  View,
  ActivityIndicator,
  SafeAreaView,
} from 'react-native';
import CardView from 'react-native-cardview';

export default class SettingsScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      mobile_no: '',
      msg: '',
    };
  }
  sendOnWhatsApp = () => {
    let url = 'whatsapp://send?text=manonjaya&phone=6285759612824';
    Linking.openURL(url)
      .then(data => {
        console.log('WhatsApp Opened');
      })
      .catch(() => {
        alert('Make sure Whatsapp installed on your device');
      });
  };

  render() {
    const {search} = this.state;
    return (
      <ScrollView>
        <Fragment>
          <Header
            statusBarProps={{barStyle: 'light-content'}}
            leftComponent={{icon: 'play-circle-outline', color: '#fff'}}
            centerComponent={{
              text: 'Product BMN',
              style: {
                color: '#fff',
                fontSize: 18,
                fontWeight: 'bold',
              },
            }}
            containerStyle={{
              backgroundColor: '#3D6DCC',
              justifyContent: 'space-around',
            }}
            rightComponent={{icon: 'local-grocery-store', color: '#fff'}}
          />
          <SafeAreaView style={styles.safeAreaView}>
            <View style={styles.container}>
              <View flexDirection="row">
                <CardView
                  cardElevation={4}
                  cardMaxElevation={4}
                  style={styles.card}>
                  <Image
                    source={{
                      uri:
                        'https://s3.amazonaws.com/uifaces/faces/twitter/brynn/128.jpg',
                    }}
                    style={{width: 200, height: 180}}
                    PlaceholderContent={<ActivityIndicator />}
                  />
                  <View style={styles.view}>
                    <Text style={styles.text}>Paket Gold Royal Anti ACNE</Text>
                    <Text style={styles.harga}>Rp. 40.000</Text>
                    <Text style={styles.kanan}>Rp. 80.000</Text>
                  </View>
                  <View flexDirection="row">
                    <Button
                      containerStyle={{
                        padding: 6,
                        height: 45,
                        overflow: 'hidden',
                        borderRadius: 4,
                        backgroundColor: 'white',
                      }}
                      icon={<Icon name="error" size={20} color="white" />}
                      onPress={this.sendOnWhatsApp}
                    />
                    <Button
                      containerStyle={{
                        padding: 6,
                        height: 45,
                        overflow: 'hidden',
                        borderRadius: 4,
                        backgroundColor: 'white',
                      }}
                      icon={<Icon name="chat" size={20} color="white" />}
                      onPress={this.sendOnWhatsApp}
                    />
                    <Button
                      containerStyle={{
                        padding: 6,
                        height: 45,
                        overflow: 'hidden',
                        borderRadius: 4,
                        backgroundColor: 'white',
                      }}
                      icon={
                        <Icon
                          name="local-grocery-store"
                          size={20}
                          color="white"
                        />
                      }
                    />
                  </View>
                </CardView>
                <CardView
                  cardElevation={4}
                  cardMaxElevation={4}
                  style={styles.card}>
                  <Image
                    source={{
                      uri:
                        'https://s3.amazonaws.com/uifaces/faces/twitter/brynn/128.jpg',
                    }}
                    style={{width: 200, height: 180}}
                    PlaceholderContent={<ActivityIndicator />}
                  />
                  <View style={styles.view}>
                    <Text style={styles.text}>Paket Gold Royal Anti ACNE</Text>
                    <Text style={styles.harga}>Rp. 40.000</Text>
                    <Text style={styles.kanan}>Rp. 80.000</Text>
                  </View>
                  <View flexDirection="row">
                    <Button
                      containerStyle={{
                        padding: 6,
                        height: 45,
                        overflow: 'hidden',
                        borderRadius: 4,
                        backgroundColor: 'white',
                      }}
                      icon={<Icon name="error" size={20} color="white" />}
                    />
                    <Button
                      containerStyle={{
                        padding: 6,
                        height: 45,
                        overflow: 'hidden',
                        borderRadius: 4,
                        backgroundColor: 'white',
                      }}
                      icon={<Icon name="chat" size={20} color="white" />}
                      onPress={this.sendOnWhatsApp}
                    />
                    <Button
                      containerStyle={{
                        padding: 6,
                        height: 45,
                        overflow: 'hidden',
                        borderRadius: 4,
                        backgroundColor: 'white',
                      }}
                      icon={
                        <Icon
                          name="local-grocery-store"
                          size={20}
                          color="white"
                        />
                      }
                    />
                  </View>
                </CardView>
              </View>
            </View>
          </SafeAreaView>
        </Fragment>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  safeAreaView: {
    flex: 1,
  },
  container: {
    flex: 1,
    // backgroundColor: '#EEEEEE',
  },
  card: {
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
    flex: 1,
    margin: 7,
  },
  text: {
    fontSize: 15,
  },
  harga: {
    fontSize: 15,
    margin: 0,
    color: 'red',
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  kanan: {
    textAlign: 'right',
    fontSize: 12,
    textDecorationLine: 'line-through',
  },
  view: {
    margin: 10,
    textAlign: 'justify',
  },
});
