import React from 'react';
import {Text} from 'react-native';
import {createSwitchNavigator, createAppContainer} from 'react-navigation';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {Provider} from 'react-redux';
import {createStore} from 'redux';
import reducers from './src/reducers';

import WelcomeScreen from './src/screens/WelcomeScreen';
import HighScoresScreen from './src/screens/HighScoresScreen';
import SettingsScreen from './src/screens/SettingsScreen';
import SplashScreen from './src/screens/SplashScreen';
import KeranjangScreen from './src/screens/KeranjangScreen';

const AppNavigator = createBottomTabNavigator(
  {
    Home: {
      screen: WelcomeScreen,
      navigationOptions: {
        tabBarLabel: ({tintColor}) => (
          <Text style={{fontSize: 10, textAlign: 'center', color: tintColor}}>
            RADIO
          </Text>
        ),
        tabBarIcon: ({horizontal, tintColor}) => (
          <Icon name="radio" size={25} color={tintColor} />
        ),
      },
    },
    HighScores: {
      screen: HighScoresScreen,
      navigationOptions: {
        tabBarLabel: ({tintColor}) => (
          <Text style={{fontSize: 10, textAlign: 'center', color: tintColor}}>
            BERITA
          </Text>
        ),
        tabBarIcon: ({horizontal, tintColor}) => (
          <Icon name="web" size={25} color={tintColor} />
        ),
      },
    },
    Keranjang: {
      screen: KeranjangScreen,
      navigationOptions: {
        tabBarLabel: ({tintColor}) => (
          <Text style={{fontSize: 10, textAlign: 'center', color: tintColor}}>
            KERANJANG
          </Text>
        ),
        tabBarIcon: ({horizontal, tintColor}) => (
          <Icon name="local-grocery-store" size={25} color={tintColor} />
        ),
      },
    },
    Settings: {
      screen: SettingsScreen,
      navigationOptions: {
        tabBarLabel: ({tintColor}) => (
          <Text style={{fontSize: 10, textAlign: 'center', color: tintColor}}>
            BMN SHOP
          </Text>
        ),
        tabBarIcon: ({horizontal, tintColor}) => (
          <Icon name="devices" size={25} color={tintColor} />
        ),
      },
    },
  },

  {
    tabBarOptions: {
      activeTintColor: '#3D6DCC',
      inactiveTintColor: '#8d8d8d',
    },
  },
);

const InitialNavigator = createSwitchNavigator({
  Splash: SplashScreen,
  App: AppNavigator,
});

const AppContainer = createAppContainer(InitialNavigator);

class App extends React.Component {
  render() {
    return (
      <Provider store={createStore(reducers)}>
        <AppContainer />
      </Provider>
    );
  }
}

export default App;
